/* eslint-disable linebreak-style */
import Router from 'express';
import Todo from '../controllers/todo'
import verifyAccessToken from '../middlewares/verifyToken';
import verifyOwnership from '../middlewares/verifyOwnership'
import todoValidations from '../validators/todo'
import catcher from '../utils/catcher'
const router = Router( )


router.get("/",verifyAccessToken,catcher(Todo.getAllTodoes));
router.patch("/:todoId", verifyAccessToken,todoValidations.updateValidation,verifyOwnership,catcher(Todo.update))
router.delete("/:todoId", verifyAccessToken,verifyOwnership,catcher(Todo.delete))
router.post("/",verifyAccessToken,todoValidations.createValidation,catcher(Todo.create))

export default router;
