/* eslint-disable linebreak-style */
import Router from 'express';
import User from '../controllers/user'
import userValidations from '../validators/user'
import verifyAccessToken from "../middlewares/verifyToken";
import catcher from '../utils/catcher';
const router = Router();


router.post("/login",User.login);
router.get("/current", verifyAccessToken, catcher(User.current));
router.post("/signup", userValidations.signUpValidation,User.signup);
router.post("/logout", verifyAccessToken, User.logout);


export default router;
