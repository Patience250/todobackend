'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Todo extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      models.Todo.belongsTo(models.User, {
        as: "user",
        foreignKey: "added_by",
    });
    }
  };
  Todo.init({
    title: DataTypes.STRING,
    description: DataTypes.STRING,
    status:  {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: "Pending",
  },
    added_by: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Todo',
    tableName:"Todos"
  });
  return Todo;
};