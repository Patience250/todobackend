'use strict';

module.exports = {
    up: (queryInterface) => queryInterface.bulkInsert('Todos', [{
        title: 'Simple todo',
        description: "First content",
        status:"pending",
        added_by:2,
        createdAt: new Date(),
        updatedAt: new Date()
    },
   
 ]),

    down: (queryInterface) => queryInterface.bulkDelete('Todos', null, {})
};