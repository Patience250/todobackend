'use strict';

module.exports = {
    up: (queryInterface) => queryInterface.bulkInsert(  "Users", [{
        first_name: "first_name",
        last_name: "last_name",
        email: "test@test.com",
        password: "$2b$10$E6fBTCtB5D3o8ynKPmoxD.zw0aaLD6s4Y49xgj2jfqQyJJID9V0Z6",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
     
]),

    down: (queryInterface) => queryInterface.bulkDelete('Users', null, {})
};