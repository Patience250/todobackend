

import {
    Todo as _Todo,
    
} from "../database/models";
import { onError } from "../utils/response";


const verifyOwnership = async (req, res, next) => {
   try {
        const todo = await _Todo.findOne({where:{id:req.params.todoId}})
        if(!todo) return onError(res, 404,"Task not found.")
        if(req.user.id !=todo.added_by)return onError(res,403,"Not allowed to change this task")
        req.todo = todo
        return next()
   } catch (error) {
       return onError(res, 500,error.message)
   }

};
export default verifyOwnership;
