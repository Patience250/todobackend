/* eslint-disable linebreak-style */
import jwt from "jsonwebtoken";
import { onError } from "../utils/response";
import client from "../config/redis_config";
import {
  User as _User
} from "../database/models";
const verifyAccessToken = async (req, res, next) => {
  const token = req.header("auth-token");
  if (!token) return onError(res, 401, "Unauthorized");
  try {
    const verified = jwt.verify(token, process.env.ACCESS_TOKEN_SECRET);
    const user = await _User.findOne({ where: { email:verified.payload.email}})
    if(!user) return onError(res,404,"User not found")
    
    client.get(verified.payload.id, (err, val) => {
      if (verified) {
        if (val) {
          req.user = verified.payload;
          return next();
        }
        return onError(
          res,
          401,
          "User already logged out, Please Login and try again!"
        );
      }
    });
  } catch (error) {
    return onError(res, 500, error.message);
  }
};
export default verifyAccessToken;
