
import {
    Todo as _Todo,
    
} from "../database/models";
import { onError, onSuccess } from '../utils/response';


export default class Todo {
    static async getAllTodoes(req, res) {
      await  _Todo.findAll({where:{added_by:req.user.id}}).then(todoes=>{
        return onSuccess(res, 200,'',todoes)
      }).catch(error=>{
        return onError(res, 404,'',"Unable to find any task.")
      })
    
     
      }
      static async create(req,res){

            const{title, description}=req.body
           await _Todo.create({title,description, added_by:req.user.id}).then(todo=>{
            return onSuccess(res,201,"Task created successfully.", todo)
              }).catch(err=>{
                  return onError(res, 500, err);
              })   
      }
      static async update(req,res){
        await req.todo.update(req.body);
        return onSuccess(res,200,`Updated successfully`, req.todo)
    }
    static async delete(req,res){
      await req.todo.destroy(req.body);
      return onSuccess(res,200,`Delete was successfull`, req.todo)
  }
}
