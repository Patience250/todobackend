
import {
    User as _User
} from "../database/models";
import signAccessToken from '../helpers/jwt_helper';
import bcrypt from 'bcrypt';
import { onError, onSuccess } from '../utils/response';
import client from "../config/redis_config";

export default class User {
    static async signup(req, res) {
        const { first_name, last_name, email, password } = req.body;
        const user = await _User.findOne({ where: { email } });
        if (user) {
          return res.status(409).json({ error: 'Email is already in use' });
        }
        try {
            await _User.create({
              first_name,
              last_name,
              email,
              password: bcrypt.hashSync(password, 8),
              isConfirmed: false,
            });
            return res.status(200).json({
              message:
                'User created successfully.',
            });
        } catch (err) {
          res.status(500).send({ error: err });
        }
    }
    static async login(req, res) {
       try {
        const user = await _User.findOne({ where: { email: req.body.email } });
        if (!user) return onError(res, 401, 'Invalid Email or Password');
        const validPassword = await bcrypt.compare(req.body.password, user.password);
        if (!validPassword) return onError(res, 401, 'Incorrect email or password');
        const token = await signAccessToken(user.dataValues);
        res.status(200).json({
          status: 200,
          accessToken: token,
          message: 'User Logged in successfully',
        }); 
       } catch (error) {
           console.log(error)
       }
      }
      static async current(req, res) {
        const { dataValues: user } = await _User.findByPk(req.user.id);
        return res.send({ data: { ...user, password: null } });
      }  
      static async logout(req, res) {
        try {
            const { id: userId } = req.user;
            client.del(userId);
            return onSuccess(res, 200, "You logged out successfully.");
        } catch (error) {
            console.log(error)
            return onError(res, 500, "Internal server error");
        }
    }
}
