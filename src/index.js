import "@babel/polyfill";
import express from 'express'
import dotenv from 'dotenv'
import fileUpload from 'express-fileupload'
import users from './routes/users'
import todoes from './routes/todoes'
import cors from 'cors';

dotenv.config();


const serverPort = process.env.PORT || 5000;

const app = express();
app.use(cors());
app.use(express.json());
app.use(fileUpload({
    createParentPath: true,
    useTempFiles: true
}))
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.listen(serverPort, () => {
    console.log(`Server has started on port ${serverPort}`)
})

app.use('/api/user', users)
app.use('/api/todoes',todoes)
export default app;