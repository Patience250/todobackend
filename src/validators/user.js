/* eslint-disable linebreak-style */
import jwt from 'jsonwebtoken';
import Joi from 'joi';


export default class userValidations {
  static signUpValidation(req, res, next) {
    const userValidationSchema = Joi.object({
      first_name: Joi.string().min(3).max(20).required(),
      last_name: Joi.string().min(3).max(20).required(),
      email: Joi.string().email().required(),
      password: Joi.string().min(8).required(),
      confirmPassword: Joi.string().valid(Joi.ref('password')).required(),
    });
    const authError = userValidationSchema.validate(req.body);
    if (authError.error) {
      return res.status(400).json({ error: authError.error.details[0].message.split('"').join('') });
    }
    return next();
  }


  static verifyToken(req, res, next) {
    try {
      const { token } = req.params;
      const decodedToken = jwt.verify(
        token,
        process.env.JWT_ACCOUNT_VEIRIFICATION
      );
      req.decoded = decodedToken;
      return next();
    } catch (err) {
      if (err.message === 'jwt malformed' || err.message === 'jwt expired') {
        return res
          .status(400)
          .json({ error: 'You are using Incorrect or Expired Link!' });
      }
      return res.status(500).json({ Error: 'Internal Error!' });
    }
  }

  static IsAllowed(role) {
    return (req, res, next) => {
      if (req.user.role !== role) {
        return res.status(403).send({ error: 'Not Allowed' });
      }
      next();
    };
  }
}
