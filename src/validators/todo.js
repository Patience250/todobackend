
import Joi from 'joi';
import { onError } from '../utils/response';

export default class todoValidations {
  static createValidation(req, res, next) {
    const todoValidationSchema = Joi.object({
      title: Joi.string().min(3).max(20).required(),
      description: Joi.string().min(3).max(20).required(),
      added_by:Joi.number(),
    });
    const todoError = todoValidationSchema.validate(req.body);
    if (todoError.error) {
      return onError(res,400, todoError.error.details[0].message.split('"').join(''))
    }
    return next();
  }
  static updateValidation(req, res, next) {
    const updateTodoValidationSchema = Joi.object({
      title: Joi.string().min(3).max(20).required(),
      description: Joi.string().min(3).max(20).required(),
      status: Joi.string().valid("started", "completed"),
      added_by:Joi.number(),
    });
    const todoError = updateTodoValidationSchema.validate(req.body);
    if (todoError.error) {
      return res.status(400).json({ error: todoError.error.details[0].message.split('"').join('') });
    }
    return next();
  }

}
